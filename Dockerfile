FROM python:3.8-alpine
RUN apk update && apk add bash jq
ENV PYTHONPATH=/python-modules


ADD requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt


ADD app /python-modules/app


CMD [ "python", "-m", "app.main" ]
