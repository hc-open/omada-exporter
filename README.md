# Omada prometheus metric exporter #

This software exports metric from the TP-Link Omada controller (both the software and the OC-200 version) ready to be scraped by a Prometheus instance

## Build ##

The software is shipped with docker, so:

` docker build -t omada-exporter .`

## Run ##

As a pre-requisite, you need to have a pair of credentials for accessing the Omada web interface. I assume you don't want to use your admin credentials, capable of controlling your wifi Access points, for a random piece of software found on the Internet, do you? So, first of all, generate a new user (role: read-only Observer) on the omada interface, and use this user for the omada-exporter

Configuration variables:

* OMADA_HOST: hostname or IP address of the omada controller
* OMADA_PORT: default 8043
* OMADA_USER
* OMADA_PASSWORD
* HTTP_BIND_PORT: default 8000   port where to expose the metrics


To run the exporter, complete and launch the following command or check the docker-compose example file:

` docker run -d --rm -p 8000:8000 -e OMADA_HOST= -e OMADA_USER= -e OMADA_PASSWORD=  omada-exporter`

Wait a couple of seconds and then browse to http://localhost:8000/metrics to check whether the metrics are exposed by the exporter.


Next steps:
Configure Prometheus for data collections, Alertmanager for alerting and Grafana for visualizing. Check their website for the instructions.


## To Do ##

Many metrics are still missing but they can be easily retrieved from the Omada API. Check how API are called in the code and feel free to submit a pull request to add new metrics or improve the code.



