import threading
import time
from abc import abstractmethod
import requests
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

import logging

log = logging.getLogger("main.omada")

logging.getLogger("requests.packages.urllib3").propagate = False


from prometheus_client import Gauge, Enum, Info, Counter

class MetricRetriever(threading.Thread):

    def __init__(self, name, retrieve_period):
        threading.Thread.__init__(self, name=name)
        self.name = name
        self.daemon = True

        self.retrieve_period = retrieve_period


    def run(self):

        log.info("Metric Retriever for metric %s started", self.name)

        while True:
            self._get_metrics()
            time.sleep(self.retrieve_period)

    @abstractmethod
    def _get_metrics(self):
        raise NotImplementedError

class MetricRetrievingError(Exception): pass


class OmadaAPMetricRetriever(MetricRetriever):

    def __init__(self, name, scraping_interval, omada_url, username, password, verify_ssl=False):
        MetricRetriever.__init__(self, name, scraping_interval)
        self.ignore_ssl = verify_ssl
        self.password = password
        self.username = username
        self.omada_url = omada_url
        self.scraping_interval = scraping_interval
        self.metrics = {}

        self._init_metrics()

    def _init_metrics(self):

        labels = ('program', 'site', 'ap_mac')
        network_labels = labels + ('interface', ) #labels for network related metrics

        self.metrics['ap_info'] = Info('ap_info', "Info about Access Point", labels)
        self.metrics['ap_radio_info'] = Info('ap_radio_info', 'Info about specific radio of an AP', network_labels)
        self.metrics['ap_status'] = Enum('ap_status', 'Access point status', labels, states=['DISCONNECTED', 'CONNECTED', 'PENDING', 'ISOLATED', 'UNKNOWN'])

        self.metrics['ap_uptime'] = Gauge('ap_uptime', "Access point uptime", labels)
        self.metrics['ap_cpu_usage'] = Gauge('ap_cpu_usage', "Access point cpu usage percentage", labels)
        self.metrics['ap_ram_usage'] = Gauge('ap_ram_usage', "Access point ram usage percentage", labels)


        self.metrics['ap_connected_client'] = Gauge('ap_connected_client', "AP Connected Client", labels)
        self.metrics['ap_connected_client_2g'] = Gauge('ap_connected_client_2g', "AP Connected Client on 2.4G radio", labels)
        self.metrics['ap_connected_client_5g'] = Gauge('ap_connected_client_5g', "AP Connected Client on 5G radio", labels)

        self.metrics['ap_last_seen_ts'] = Gauge('ap_last_seen_ts', "Ap last seen timestamp", labels)

        self.metrics['ap_download'] = Gauge('ap_download', "Ap Downloaded bytes", labels)
        self.metrics['ap_upload'] = Gauge('ap_upload', "Ap Uploaded bytes", labels)

        self.metrics['ap_channel_rx_util'] = Gauge('ap_channel_rx_util', "Radio Bandwidth used for receiving frames", network_labels)
        self.metrics['ap_channel_tx_util'] = Gauge('ap_channel_tx_util', "Radio Bandwidth used for transmitting frames", network_labels)
        self.metrics['ap_channel_interference'] = Gauge('ap_channel_interference', "Radio Bandwidth interference percentage", network_labels)
        self.metrics['ap_channel_busy'] = Gauge('ap_channel_busy', "Radio Bandwidth busy percentage", network_labels)

        self.metrics['ap_rx_packets'] = Gauge('ap_rx_packets', "Received packets on a specific ap interface", network_labels)
        self.metrics['ap_tx_packets'] = Gauge('ap_tx_packets', "Transmitted packets on a specific ap interface", network_labels)
        self.metrics['ap_rx_bytes'] = Gauge('ap_rx_bytes', "Received bytes on a specific ap interface", network_labels)
        self.metrics['ap_tx_bytes'] = Gauge('ap_tx_bytes', "Transmitted bytes on a specific ap interface", network_labels)
        self.metrics['ap_rx_dropped_packets'] = Gauge('ap_rx_dropped_packets', "Dropped received packets on a specific ap interface", network_labels)
        self.metrics['ap_tx_dropped_packets'] = Gauge('ap_tx_dropped_packets', "Dropped transmitted packets on a specific ap interface", network_labels)
        self.metrics['ap_tx_errors'] = Gauge('ap_tx_errors', "Tx errors on a specific ap interface", network_labels)
        self.metrics['ap_rx_errors'] = Gauge('ap_rx_errors', "Rx errors on a specific ap interface", network_labels)

        labels = ('program', 'ssid')
        self.metrics['ssid_client_total'] = Gauge('ssid_client_total', "Total number of connected client on an SSID", labels)
        self.metrics['ssid_upload_total'] = Gauge('ssid_upload_total', "Total uploaded traffic on an SSID", labels)
        self.metrics['ssid_download_total'] = Gauge('ssid_download_total', "Total uploaded traffic on an SSID", labels)




    def __repr__(self):
        return "<OmadaAPMetricRetriever %s, getting from %s>" % (self.name, self.scraping_interval)

    def _get_metrics(self):

        with requests.session() as session:
            token = self._get_new_auth_token(session)

            ssid_metrics = self._get_ssid_metrics(session, token)

            self._update_ssid_metric(ssid_metrics)

            ap_list = self._get_access_point_list(session, token)

            for ap in ap_list:
                ap_stat = self._get_single_ap_metrics(session, token, ap)
                self._update_metric_of_single_ap(ap_stat)



    def _get_new_auth_token(self, request_session: requests.Session):
        auth_url = "%s%s" % (self.omada_url, "api/user/login?ajax")
        auth_cmd = {
            "method": "login",
            "params": {
                "name": self.username,
                "password": self.password
            }
        }
        resp = request_session.post(auth_url, json=auth_cmd, verify=self.ignore_ssl)

        if resp.status_code != 200:
            raise MetricRetrievingError("Unable to login to omada controller, %s" % resp.text)

        token = resp.json()['result']['token']

        return token

    def _get_access_point_list(self, session: requests.Session, token: str):
        ap_list_url = "%s%s" % (self.omada_url, "web/v1/controller")
        ap_list_command = {
            "method": "getGridAps",
            "params": {
                "sortOrder": "asc",
                "currentPage": 1,
                "currentPageSize": 100000,  # TODO for very large deployment the response could be huge
                "filters": {"status": "All"}
            }
        }

        resp = session.post(ap_list_url,
                            params={"token": token},
                            json=ap_list_command,
                            verify=self.ignore_ssl
                            )

        if resp.status_code != 200 or resp.headers.get('content-type') != 'application/json;charset=UTF-8':
            raise MetricRetrievingError("Error in retrieving ap list")

        body = resp.json()

        ap_list = [i['mac'] for i in body['result']['data']]

        log.debug("Retrieved ap list: %s", ap_list)

        return ap_list

    def _get_single_ap_metrics(self, session: requests.Session, token: str, mac: str):
        ap_single_stat_url = "%s%s" % (self.omada_url, "web/v1/controller")
        single_stat_cmd = {"method": "getApDetail",
                           "params": {
                               "apMac": mac
                           }
                           }

        resp = session.post(ap_single_stat_url,
                            params={"token": token},
                            json=single_stat_cmd,
                            verify=self.ignore_ssl
                            )
        if resp.status_code != 200 or resp.headers.get('content-type') != 'application/json;charset=UTF-8':
            raise MetricRetrievingError("Error in retrieving ap detailed statistics")

        return resp.json()['result']

    def _get_ssid_metrics(self, session: requests.Session, token: str):
        ssid_stat_url = "%s%s" % (self.omada_url, "web/v1/controller")
        ssid_stat_cmd = {"method":"getSsidStats","params":{}}

        resp = session.post(ssid_stat_url,
                            params={"token": token},
                            json=ssid_stat_cmd,
                            verify=self.ignore_ssl
                            )
        if resp.status_code != 200 or resp.headers.get('content-type') != 'application/json;charset=UTF-8':
            raise MetricRetrievingError("Error in retrieving ssid statistics")

        return resp.json()['result']


    def _update_metric_of_single_ap(self, ap_stat: dict):
        log.debug(ap_stat)
        stat = ap_stat

        site = stat['site']
        ap_mac = stat['mac']
        labels = ('omada', site, ap_mac)

        self.metrics['ap_info'].labels(*labels).info({
            "model": stat['model'],
            "name": stat['name'],
            "model_version": stat['modelModelVersion'],
            "hw_version": stat['hwVersion'],
            "omada_id": stat['id'],
            "sw_version": stat['version'],
            "ip": stat['ip']
        })
        self.metrics['ap_status'].labels(*labels).state(self._get_status_label(ap_stat))

        uptime = stat['sca']['uptime'] if stat['sca'] is not None else -1
        self.metrics['ap_uptime'].labels(*labels).set(uptime)
        cpu = stat['sca']['cpuRate'] if stat['sca'] is not None else -1
        self.metrics['ap_cpu_usage'].labels(*labels).set(cpu)
        ram = stat['sca']['memoryRate'] if stat['sca'] is not None else -1
        self.metrics['ap_ram_usage'].labels(*labels).set(ram)

        self.metrics['ap_connected_client'].labels(*labels).set(stat['clientNum'])
        self.metrics['ap_connected_client_2g'].labels(*labels).set(stat['clientNum2g'])
        self.metrics['ap_connected_client_5g'].labels(*labels).set(stat['clientNum5g'])

        self.metrics['ap_last_seen_ts'].labels(*labels).set(stat['lastSeen'])
        self.metrics['ap_download'].labels(*labels).set(stat['download'])
        self.metrics['ap_upload'].labels(*labels).set(stat['upload'])

        if stat['sca'] is not None:
            self._update_interface_stats(stat['sca']['lanTraffic'], labels + ('lan', ))

            if stat['sca']['radioTraffic'] is not None:
                net_label = labels + ('2g',)
                self._update_interface_stats(stat['sca']['radioTraffic'], net_label)
                if stat['wp2g'] is not None:
                    rx = stat['wp2g']['rxUtil'] if stat['wp2g']['rxUtil'] is not None else 0
                    self.metrics['ap_channel_rx_util'].labels(*net_label).set(rx)
                    tx = stat['wp2g']['txUtil'] if stat['wp2g']['txUtil'] is not None else 0
                    self.metrics['ap_channel_tx_util'].labels(*net_label).set(tx)
                    interference = stat['wp2g']['interUtil'] if stat['wp2g']['interUtil'] is not None else 0
                    self.metrics['ap_channel_interference'].labels(*net_label).set(interference)
                    busy = stat['wp2g']['busyUtil'] if stat['wp2g']['busyUtil'] is not None else 0
                    self.metrics['ap_channel_busy'].labels(*net_label).set(busy)
                    self.metrics['ap_radio_info'].labels(*net_label).info(self._create_radio_info_dict(stat['wp2g']))

            if stat['sca']['radioTraffic5g'] is not None:
                net_label = labels + ('5g',)
                self._update_interface_stats(stat['sca']['radioTraffic'], net_label)
                if stat['wp5g'] is not None:
                    rx = stat['wp5g']['rxUtil'] if stat['wp5g']['rxUtil'] is not None else 0
                    self.metrics['ap_channel_rx_util'].labels(*net_label).set(rx)
                    tx = stat['wp5g']['txUtil'] if stat['wp5g']['txUtil'] is not None else 0
                    self.metrics['ap_channel_tx_util'].labels(*net_label).set(tx)
                    interference = stat['wp5g']['interUtil'] if stat['wp5g']['interUtil'] is not None else 0
                    self.metrics['ap_channel_interference'].labels(*net_label).set(interference)
                    busy = stat['wp5g']['busyUtil'] if stat['wp5g']['busyUtil'] is not None else 0
                    self.metrics['ap_channel_busy'].labels(*net_label).set(busy)
                    self.metrics['ap_radio_info'].labels(*net_label).info(self._create_radio_info_dict(stat['wp5g']))



    def _update_interface_stats(self, interface_statistics: dict, labels: tuple):
        intstat = interface_statistics

        self.metrics['ap_rx_packets'].labels(*labels).set(intstat['rxPackets'])
        self.metrics['ap_tx_packets'].labels(*labels).set(intstat['txPackets'])
        self.metrics['ap_rx_bytes'].labels(*labels).set(intstat['rxBytes'])
        self.metrics['ap_tx_bytes'].labels(*labels).set(intstat['txBytes'])
        self.metrics['ap_rx_dropped_packets'].labels(*labels).set(intstat['rxDroppedPackets'])
        self.metrics['ap_tx_dropped_packets'].labels(*labels).set(intstat['txDroppedPackets'])
        self.metrics['ap_rx_errors'].labels(*labels).set(intstat['rxErrors'])
        self.metrics['ap_tx_errors'].labels(*labels).set(intstat['txErrors'])

    def _create_radio_info_dict(self, radio_stat: dict):
        return {
            'channel': radio_stat['actualChannel'],
            'channel_width': radio_stat['bandWidth'],
            'channel_tx_rate': str(radio_stat['maxTxRate']),
            'channel_tx_power': str(radio_stat['txPower'])
        }

    def _get_status_label(self, ap_stat: dict):
        ap_status = "UNKNOWN"

        if ap_stat['status'] == 0:
            ap_status = "DISCONNECTED"
        elif ap_stat['status'] == 1:
            ap_status = "CONNECTED"
        elif ap_stat['status'] == 2:
            ap_status = "PENDING"
        elif ap_stat['status'] == 4:
            ap_status = "ISOLATED"

        return ap_status

    def _update_ssid_metric(self, ssid_metrics: dict):

        for ssid in ssid_metrics['ssidList']:
            labels = ('omada', ssid['ssid'])
            self.metrics['ssid_client_total'].labels(*labels).set(ssid['totalClient'])
            self.metrics['ssid_upload_total'].labels(*labels).set(ssid['totalUpload'])
            self.metrics['ssid_download_total'].labels(*labels).set(ssid['totalDownload'])
